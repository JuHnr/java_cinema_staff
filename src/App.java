import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
       String[] employeeNames = { "Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly" };
        int[] hoursWorked = { 35, 38, 35, 38, 40 };
        double[] hourlyRates = { 12.5, 15.0, 13.5, 14.5, 13.0 };
        String[] positions = { "Caissier", "Projectionniste", "Caissier", "Manager", "Caissier" };
        int maxWorkingHours = 35;

        for (int i = 0; i < employeeNames.length; i++) {
            
            System.out.println(employeeNames[i] + " salary : ");

            double salary;
            // If the employee did extra hours, apply a multiplier of 1.5 to the hourly rate
            // for those.
            if (hoursWorked[i] > maxWorkingHours) {
                int extraHours = hoursWorked[i] - maxWorkingHours;
                salary = extraHours * (hourlyRates[i] * 1.5) + hoursWorked[i] * hourlyRates[i];
            } else {
                salary = hoursWorked[i] * hourlyRates[i];
            }

            System.out.println(salary);
        }

        String searchPosition = "Caissier";
        ArrayList<String> resultForSearchPosition = new ArrayList<>();

        for (int i = 0; i < employeeNames.length; i++) {
            if (positions[i].equals(searchPosition)) {
                resultForSearchPosition.add(employeeNames[i]);
            }
        }

        if (resultForSearchPosition.isEmpty()) {
            System.out.println("Aucun employé trouvé.");
        } else {
            System.out.println("Caissier(s) : " + resultForSearchPosition);
        }

    }
}
